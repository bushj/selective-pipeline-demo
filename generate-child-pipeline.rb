#! /usr/bin/env ruby

env_include_files = ENV["INCLUDE_FILES"]
include_files = env_include_files.nil? ? [] : env_include_files.split

if !include_files.empty?
    puts "include:"
    include_files.each { |f| puts "- local: '#{f}'" }
end